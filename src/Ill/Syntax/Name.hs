module Ill.Syntax.Name where

import Ill.Prelude

type Name = String
type Id = String
